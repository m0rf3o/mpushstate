/* =====================================================================
 * mPushState.js v1.2
 * https://bitbucket.org/m0rf3o/mpushstate
 * =====================================================================
 * Copyright (c) 2012 Nestor Miguel Flores Estrada <nestorflores87@gmail.com> 
 * http://www.dataflood.blogspot.com
 * 
 * 
 * Licensed under the MIT licenses:
 * http://www.opensource.org/licenses/mit-license.php
 * 
 * 
 */



var mPushState = function() {
    /*Creando el contenedor de las variables*/
    this.vars = new Array();

    /*Verificacion si el navegador soporta pushtate*/
    if (typeof window.history.pushState == 'function') {
        this.supportPush = true;
    } else {
        this.supportPush = false;
    }
    //Devuelve el nombre del script ya sea ie o ff
    cont = window.location.href.split('/').slice(-1).toString().split(/\?|#!|&/);
    this.script = cont[0];

    for (var i = 1; i < cont.length; i++) {
        var divs = cont[i].split('=');
        //Si es un par nombre=valor que la agregue a las variables actuales
        if (divs.length == 2) {
            this.vars[divs[0]] = divs[1];
        }
    }
}

mPushState.prototype.adVar = function(name, value) {
    this.vars[name] = value;

    this.reloadUrl();

}

mPushState.prototype.delVar = function(name) {

    delete this.vars[name];
    this.reloadUrl();

}
mPushState.prototype.reloadUrl = function() {
    var nUrl = '';
    var contador = 0;//Servira para encontrar la posicion de las variables
    if (this.supportPush) {
        nUrl += this.script;
        for (var key in this.vars) {
            nUrl += (contador == 0) ? '?' : '&';
            nUrl += key + '=' + this.vars[key];
            contador++;
        }
        history.pushState({path: nUrl}, nUrl, nUrl);
    } else {
        for (var key in this.vars) {
            nUrl += (contador == 0) ? '#!' : '&';
            nUrl += key + '=' + this.vars[key];
            contador++;
        }
        window.location.hash = nUrl;
    }
}


mPushState.prototype.getVar = function(name) {
    return this.vars[name];
}